﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Helper;
using Dapper;
using Model.PartialModel;

namespace DAO
{
    public class BlogDAO
    {
        public List<Blog> GetBlogs(string name = "", string description = "",string startDate = "", string endDate = "", int status = 1)
        {
            var listData = new List<Blog>();
            string strQuery = string.Format("Select Blog.*, Author.name as AuthorName from Blog left join Author on Blog.AuthorId = Author.Id where  Blog.name = '{0}'", name);
            if (!string.IsNullOrEmpty(description))
            {
                strQuery += string.Format(" and [Description] = '{0}'", description);
            }
            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                strQuery += string.Format(" and  [CreateDate] >= '{0}' and [CreateDate] <= '{1}'", DateTime.Parse(startDate.Trim()).ToString("yyyy-MM-dd"), DateTime.Parse(endDate.Trim()).ToString("yyyy-MM-dd"));
            }
            if (!string.IsNullOrEmpty(status.ToString()))
            {
                strQuery += string.Format(" and [Status] = {0}", status);
            }
            using (IDbConnection sqlCon = new SqlConnection(HelperDAO.GetConnection()))
            {
                listData = sqlCon.Query<Blog>(strQuery).ToList();
            }
            return listData;
        }

        public void Insert(Blog blog)
        {
            string strQuery = string.Format("INSERT INTO Blog VALUES('{0}','{1}','{2}','{3}','{4}',{5})", blog.Name, blog.Description, blog.Status, blog.CreateDate, blog.Image, blog.AuthorId);
            HelperDAO.ExecuteNonQueryText(strQuery);
        }
    }
}
