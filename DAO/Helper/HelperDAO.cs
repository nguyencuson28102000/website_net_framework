﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Helper
{
    public class HelperDAO
    {
        private const string ConnectionStringDB = "DbContext";
        public static string GetConnection()
        {
            if (ConfigurationManager.ConnectionStrings["DbContext"] != null)
                return ConfigurationManager.ConnectionStrings["DbContext"].ConnectionString;
            return string.Empty;
        }

        public static object ExecuteScalarText(string Query)
        {
            SqlCommand myCommand = new SqlCommand();
            SqlConnection connection = new SqlConnection(GetConnection());
            try
            {
                connection.Open();
                myCommand.Connection = connection;
                myCommand.CommandText = Query;
                myCommand.CommandType = CommandType.Text;
                return myCommand.ExecuteScalar();
            }
            catch (SqlException)
            {
                return -1;
            }
            finally
            {
                connection.Close();
            }
        }

        public static int ExecuteNonQueryText(string _store)
        {
            SqlCommand myCommand = new SqlCommand();
            SqlConnection connection = new SqlConnection(GetConnection());
            try
            {
                connection.Open();
                myCommand.Connection = connection;
                myCommand.CommandText = _store;
                myCommand.CommandType = CommandType.Text;
                return myCommand.ExecuteNonQuery();
            }
            catch (SqlException)
            {
                if (connection != null) connection.Close();
                return -1;
            }
            finally
            {
                connection.Close();
            }
        }

        public static DataSet ExecuteDataset(String _store, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            SqlConnection connection = new SqlConnection(GetConnection());
            try
            {
                connection.Open();
                myCommand.Connection = connection;
                myCommand.CommandText = _store;
                myCommand.CommandType = CommandType.StoredProcedure;
                if (sqlParameter != null)
                {
                    myCommand.Parameters.AddRange(sqlParameter);
                }
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
            }
            catch (SqlException)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
            return ds;
        }
        public static DataSet ExecuteDataset_Text(string sqlQuery)
        {
            SqlCommand myCommand = new SqlCommand();
            DataSet ds = new DataSet();
            SqlConnection connection = new SqlConnection(GetConnection());
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            try
            {
                connection.Open();
                myCommand.Connection = connection;
                myCommand.CommandText = sqlQuery;
                myCommand.CommandType = CommandType.Text;
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
            }
            catch (SqlException)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
            return ds;

        }
    }
}
