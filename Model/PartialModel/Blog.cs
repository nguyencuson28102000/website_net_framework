﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.PartialModel
{
    public partial class Blog
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public DateTime CreateDate { get; set; }  
        public string Image { get; set; }
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }

        public string CreateDateStr
        {
            get
            {
                return CreateDate.ToShortDateString();
            }
        }
    }
}
