﻿using DAO;
using Model.PartialModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private BlogDAO blogDAO = new BlogDAO();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Search_Info(string name, string description, int status, string startDate, string endDate)
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var listData = blogDAO.GetBlogs(name, description, startDate, endDate, status);
                for (int i = 0; i < listData.Count; i++)
                {
                    listData = listData.OrderBy(x => x.Id).ToList();
                }
                listData = listData.OrderBy(x => x.Id).ToList();
                recordsTotal = listData.Count();
                var data = listData.Skip(skip).Take(pageSize).ToList();

                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data.OrderBy(x => x.Id).ThenBy(n => n.Name) });
            }
            catch (Exception e)
            {
                throw;
            }

        }

        public JsonResult AddBlog(Blog blog)
        {
            try
            {
                blogDAO.Insert(blog);
                return Json(new { blog = blog });
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}